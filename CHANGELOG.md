# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

### Changed

-   ci/cd postgres image reference ([core#125](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/125))

## [2.22.0] - 2025-02-10

-   No changelog

## [2.21.1] - 2025-02-06

-   No changelog

## [2.21.0] - 2025-01-29

-   No changelog

## [2.20.0] - 2025-01-27

-   No changelog

## [2.19.1] - 2025-01-22

-   No changelog

## [2.19.0] - 2025-01-20

### Changed

-   Change TS build target from ES2015 to ES2021 ([core#121](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/121))

## [2.18.0] - 2025-01-13

-   No changelog

## [2.17.2] - 2025-01-13

### Changed

-   Optimize CI/CD and builds of Docker images ([core#126](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/126))
-   Move Golemio CLI to optional dependencies ([core#126](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/126))

## [2.17.1] - 2025-01-13

-   No changelog

## [2.17.0] - 2025-01-13

-   No changelog

## [2.16.1] - 2024-12-18

-   No changelog

## [2.16.0] - 2024-12-17

### Changed

-   move golemio/cli to dev dependencies ([golemio-cli#3](https://gitlab.com/operator-ict/golemio/code/golemio-cli/-/issues/3))
-   Disable Sentry Express middleware ([p0255#90](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/90))

### Fixed

-   Memory leaks related to Sentry Express middleware ([p0255#90](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/90))

## [2.15.2] - 2024-12-04

### Changed

-   Update Node.js to v20.18.0 ([core#119](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/119))

## [2.15.1] - 2024-11-27

-   No changelog

## [2.15.0] - 2024-11-21

### Changed

-   Azure Blob Storage managed identity env vars update ([infra#304](https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/issues/304))

## [2.14.6] - 2024-11-20

-   No changelog

## [2.14.5] - 2024-11-13

-   No changelog

## [2.14.4] - 2024-11-11

-   No changelog

## [2.14.3] - 2024-11-06

-   No changelog

## [2.14.2] - 2024-11-04

-   No changelog

## [2.14.1] - 2024-10-23

### Added

-   asyncAPI upload documentation to Azure Blob Storage ([integration-engine#267](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/267))

## [2.14.0] - 2024-10-14

-   No changelog

## [2.13.1] - 2024-10-01

-   No changelog

## [2.13.0] - 2024-09-30

-   No changelog

## [2.12.5] - 2024-09-18

-   No changelog

## [2.12.4] - 2024-09-09

-   No changelog

## [2.12.3] - 2024-09-02

### Added

-   asyncAPI merge yaml files ([integration-engine#258](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/258))

## [2.12.2] - 2024-08-19

-   No changelog

## [2.12.1] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [2.12.0] - 2024-08-14

-   No changelog

## [2.11.0] - 2024-08-07

-   No changelog

## [2.10.0] - 2024-07-31

-   No changelog

## [2.9.8] - 2024-07-29

### Added

-   Ropid static data to blob storage[pid#329](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/329)

## [2.9.7] - 2024-07-10

-   No changelog

## [2.9.6] - 2024-07-04

-   No changelog

## [2.9.5] - 2024-07-01

-   No changelog

## [2.9.4] - 2024-06-24

### Added

-   Asyncapi documentation [pid#372](https://gitlab.com/operator-ict/golemio/projekty/ropid/polohy-vozidel/-/issues/372)

## [2.9.3] - 2024-06-12

-   No changelog

## [2.9.2] - 2024-06-05

-   No changelog

## [2.9.1] - 2024-06-05

-   No changelog

## [2.9.0] - 2024-06-03

-   No changelog

## [2.8.10] - 2024-05-29

-   No changelog

## [2.8.9] - 2024-05-22

-   No changelog

## [2.8.8] - 2024-05-20

-   No changelog

## [2.8.7] - 2024-05-15

-   No changelog

## [2.8.6] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [2.8.5] - 2024-05-09

-   No changelog

## [2.8.4] - 2024-05-06

-   No changelog

## [2.8.3] - 2024-04-29

-   No changelog

## [2.8.2] - 2024-04-15

-   No changelog

## [2.8.1] - 2024-04-15

-   No changelog

## [2.8.0] - 2024-04-10

-   No changelog

## [2.7.14] - 2024-04-03

-   No changelog

## [2.7.13] - 2024-03-27

-   No changelog

## [2.7.12] - 2024-03-25

-   No changelog

## [2.7.11] - 2024-03-11

-   No changelog

## [2.7.10] - 2024-03-06

-   No changelog

## [2.7.9] - 2024-03-04

-   No changelog

## [2.7.8] - 2024-02-21

-   No changelog

## [2.7.7] - 2024-02-14

### Changed

-   Modify module loader to improve logging ([core#92](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/92))

### Changed

-   Rename datasources ([core#75](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/75))

## [2.7.6] - 2024-02-12

-   No changelog

## [2.7.5] - 2024-02-05

### Changed

-   Azure table credentials ([core#88](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/88))

## [2.7.4] - 2024-01-29

-   No changelog

## [2.7.3] - 2024-01-22

-   No changelog

## [2.7.2] - 2024-01-17

-   No changelog

## [2.7.1] - 2024-01-10

-   No changelog

## [2.7.0] - 2023-12-20

-   No changelog

## [2.6.10] - 2023-12-18

-   No changelog

## [2.6.9] - 2023-12-13

-   No changelog

## [2.6.8] - 2023-12-11

### Added

-   Azure Table Storage environment variables ([core#84](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/84))

## [2.6.7] - 2023-12-06

-   No changelog

## [2.6.6] - 2023-11-29

-   No changelog

## [2.6.5] - 2023-11-22

-   No changelog

## [2.6.4] - 2023-11-15

-   No changelog

## [2.6.3] - 2023-11-13

-   No changelog

## [2.6.2] - 2023-10-23

-   No changelog

## [2.6.1] - 2023-10-18

-   No changelog

## [2.6.0] - 2023-10-09

### Changed

-   CI/CD - Update Redis services to v7.2.1

## [2.5.10] - 2023-10-02

-   No changelog

## [2.5.9] - 2023-09-11

-   No changelog

## [2.5.8] - 2023-09-06

-   No changelog

## [2.5.7] - 2023-09-04

-   No changelog

## [2.5.6] - 2023-08-30

-   No changelog

## [2.5.5] - 2023-08-21

### Added

-   `RABBIT_CONSUMER_TIMEOUT` env for abortable workers

## [2.5.4] - 2023-08-07

-   No changelog

## [2.5.3] - 2023-08-02

-   No changelog

## [2.5.2] - 2023-07-31

### Changed

-   Node 18.17

## [2.5.1] - 2023-07-26

-   No changelog

## [2.5.0] - 2023-07-24

-   No changelog

## [2.4.12] - 2023-07-20

-   No changelog

## [2.4.11] - 2023-07-17

### Fixed

-   Open telemetry initialization

## [2.4.10] - 2023-07-10

-   No changelog

## [2.4.9] - 2023-06-26

-   No changelog

## [2.4.8] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [2.4.7] - 2023-06-07

-   No changelog

## [2.4.6] - 2023-05-31

### Changed

-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))

## [2.4.5] - 2023-05-29

-   No changelog

## [2.4.4] - 2023-05-25

-   No changelog

## [2.4.3] - 2023-05-22

-   No changelog

## [2.4.2] - 2023-05-17

-   No changelog

## [2.4.1] - 2023-05-10

-   No changelog

## [2.4.0] - 2023-05-03

-   No changelog

## [2.3.8] - 2023-04-26

-   No changelog

## [2.3.7] - 2023-04-19

-   No changelog

## [2.3.6] - 2023-04-12

-   No changelog

## [2.3.5] - 2023-03-29

-   No changelog

## [2.3.4] - 2023-03-27

### Added

-   Vehicle descriptor datasource config

## [2.3.3] - 2023-03-16

No changelog

## [2.3.2] - 2023-03-15

No changelog

## [2.3.1] - 2023-03-01

No changelog

## [2.3.0] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [2.2.0] - 2023-02-13

No changelog

## [2.1.1] - 2023-01-30

No changelog

## [2.1.0] - 2023-01-23

### Changed

-   Docker image optimization
-   Migrate to npm

## [2.0.52] - 2023-01-16

No changelog

## [2.0.51] - 2023-01-04

No changelog

## [2.0.50] - 2022-12-13

No changelog

## [2.0.49] - 2022-11-29

No changelog

## [2.0.48] - 2022-11-10

No changelog

## [2.0.47] - 2022-10-13

### Changed

-   Update TypeScript to v4.7.2

## [2.0.46] - 2022-10-05

No changelog

## [2.0.45] - 2022-10-04

### Changed

-   Update Node.js to 16.17.0 ([code/general#418](https://gitlab.com/operator-ict/golemio/code/general/-/issues/418))

### Removed

-   Datasource tests ([pid#174](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/174))
-   Unused dependencies ([modules/general#5](https://gitlab.com/operator-ict/golemio/code/modules/general/-/issues/5))

