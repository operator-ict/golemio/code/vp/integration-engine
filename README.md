[![pipeline status](https://gitlab.com/operator-ict/golemio/code/vp/integration-engine/badges/master/pipeline.svg)](https://gitlab.com/operator-ict/golemio/code/vp/integration-engine/commits/master)
[![coverage report](https://gitlab.com/operator-ict/golemio/code/vp/integration-engine/badges/master/coverage.svg)](https://gitlab.com/operator-ict/golemio/code/vp/integration-engine/commits/master)

# Golemio VP Integration Engine

Fork of [Integration Engine](https://gitlab.com/operator-ict/golemio/code/integration-engine) dedicated only to vehicle positions.
