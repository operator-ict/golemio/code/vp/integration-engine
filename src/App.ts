import { BaseApp, getServiceHealth, ILogger, IServiceCheck, Service } from "@golemio/core/dist/helpers";
import { IRedisConnector } from "@golemio/core/dist/helpers/data-access/redis/IRedisConnector";
import { IConfiguration } from "@golemio/core/dist/integration-engine";
import { AMQPConnector } from "@golemio/core/dist/integration-engine/connectors/AMQPConnector";
import IPostgresConnector from "@golemio/core/dist/integration-engine/connectors/interfaces/IPostgresConnector";
import { ContainerToken, IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc";
import { filterQueueDefinitions, QueueProcessor } from "@golemio/core/dist/integration-engine/queueprocessors";
import { initSentry, metricsService } from "@golemio/core/dist/monitoring";
import express, { NextFunction, Request, RequestHandler, Response } from "@golemio/core/dist/shared/express";
import {
    ErrorHandler,
    FatalError,
    GeneralError,
    HTTPErrorHandler,
    IGolemioError,
} from "@golemio/core/dist/shared/golemio-errors";
import { createLightship, Lightship } from "@golemio/core/dist/shared/lightship";
import { pinoHttp } from "@golemio/core/dist/shared/pino";
import sentry from "@golemio/core/dist/shared/sentry";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import http from "http";
import path from "path";
import { ModuleLoader } from "./ModuleLoader";
@injectable()
export default class App extends BaseApp {
    public express: express.Application = express();
    public server?: http.Server;
    public metricsServer?: http.Server;
    public port: number;
    private commitSHA: string | undefined = undefined;
    private lightship: Lightship;
    private queueProcessors: Set<QueueProcessor>;

    /**
     * Run configuration methods on the Express instance
     * and start other necessary services (crons, database, middlewares).
     */
    constructor(
        @inject(ContainerToken.Config) private readonly config: IConfiguration,
        @inject(ContainerToken.Logger) private readonly log: ILogger,
        @inject(ContainerToken.AmqpConnector) private readonly amqpConnector: AMQPConnector,
        @inject(ContainerToken.PostgresConnector) private readonly postgresConnector: IPostgresConnector,
        @inject(ContainerToken.RedisConnector) private readonly redisConnector: IRedisConnector,
        @inject(ContainerToken.RequestLogger) private readonly requestLogger: pinoHttp.HttpLogger
    ) {
        super();
        this.port = parseInt(config.port || "3006", 10);
        this.lightship = createLightship({
            detectKubernetes: config.NODE_ENV !== "production",
            shutdownHandlerTimeout: config.lightship.handlerTimeout,
            gracefulShutdownTimeout: config.lightship.shutdownTimeout,
            shutdownDelay: config.lightship.shutdownDelay,
        });
        process.on("uncaughtException", (err: Error) => {
            log.error(err);
            this.lightship.shutdown();
        });
        process.on("unhandledRejection", (err: Error) => {
            log.error(err);
            this.lightship.shutdown();
        });

        this.queueProcessors = new Set();
    }

    /**
     * Start the application
     */
    public start = async (): Promise<void> => {
        try {
            initSentry(this.config.sentry, this.config.app_name);
            metricsService.init(this.config, this.log);
            this.metricsServer = metricsService.serveMetrics();
            await this.expressServer();
            this.commitSHA = await this.loadCommitSHA();
            this.log.info(`Commit SHA: ${this.commitSHA}`);
            await this.database();
            await this.registerQueues();
            this.log.info("Started!");
            this.lightship.registerShutdownHandler(async () => {
                await this.gracefulShutdown();
            });
            this.lightship.signalReady();
        } catch (err) {
            sentry.captureException(err);
            ErrorHandler.handle(err, this.log);
        }
    };

    /**
     * Start the database connection with initial configuration
     */
    private database = async (): Promise<void> => {
        await this.postgresConnector.connect();
        await this.redisConnector.connect();
    };

    /**
     * Graceful shutdown - terminate connections and server
     */
    private gracefulShutdown = async (): Promise<void> => {
        this.log.info("Graceful shutdown initiated.");
        await this.cancelConsumers();
        await IntegrationEngineContainer.dispose();
        await this.server?.close();
        await this.metricsServer?.close();
    };

    /**
     * Start the message queue connection, create communication channel
     * and register queue processors to consume messages
     */
    private registerQueues = async (): Promise<void[]> => {
        let modules = await ModuleLoader.loadModules();
        const registeredQueues = [];

        const channel = await this.amqpConnector.connect();

        for (const moduleName in modules) {
            // { queueDefinitions, workers }
            // Instantiate workers and generate queue definitions
            const queueDefinitions = [...modules[moduleName].queueDefinitions];
            for (const Worker of modules[moduleName].workers) {
                const worker = new Worker();
                queueDefinitions.push(worker.getQueueDefinition());
            }

            const filteredQueueDefinitions = filterQueueDefinitions(queueDefinitions, this.config.queuesBlacklist);

            registeredQueues.push(
                ...filteredQueueDefinitions.map((queueDefinition) => {
                    const queueProcessor = new QueueProcessor(channel, queueDefinition, moduleName);
                    this.queueProcessors.add(queueProcessor);
                    return queueProcessor.registerQueues();
                })
            );
        }

        return Promise.all(registeredQueues);
    };

    /**
     * Cancel all consumer operations before shutting down
     */
    private cancelConsumers = async (): Promise<void> => {
        for (const queueProcessor of this.queueProcessors.values()) {
            await queueProcessor.cancelConsumers();
        }
    };

    /**
     * Set custom headers
     */
    private customHeaders = (_req: Request, res: Response, next: NextFunction) => {
        res.setHeader("Access-Control-Allow-Methods", "GET, OPTIONS, HEAD");
        next();
    };

    /**
     * Start the express server
     */
    private async expressServer(): Promise<void> {
        this.middleware();
        this.routes();
        this.errorHandlers();

        this.server = http.createServer(this.express);
        // Setup error handler hook on server error
        this.server.on("error", (err) => {
            sentry.captureException(err);
            ErrorHandler.handle(new FatalError("Could not start a server", "App", err), this.log);
        });
        // Serve the application at the given port
        this.server.listen(this.port, () => {
            // Success callback
            this.log.info(`Listening at http://localhost:${this.port}/`);
        });
    }

    /**
     * Bind middleware to express server
     */
    private middleware = (): void => {
        // TODO: Sentry middleware cause memory issues related to
        //   https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/90
        // this.express.use(sentry.Handlers.requestHandler() as RequestHandler);
        // this.express.use(sentry.Handlers.tracingHandler() as RequestHandler);

        this.express.use(this.requestLogger);
        this.express.use(this.commonHeaders);
        this.express.use(this.customHeaders);
    };

    private healthCheck = async () => {
        const description = {
            app: "Golemio Data Platform Integration Engine",
            commitSha: this.commitSHA,
            version: this.config.app_version,
        };

        const services: IServiceCheck[] = [
            { name: Service.POSTGRES, check: this.postgresConnector.isConnected },
            { name: Service.REDIS, check: this.redisConnector.isConnected },
            { name: Service.RABBITMQ, check: this.amqpConnector.isConnected },
        ];

        const serviceStats = await getServiceHealth(services);

        return { ...description, ...serviceStats };
    };

    /**
     * Define express server routes
     */
    private routes = (): void => {
        const defaultRouter = express.Router();

        // base url route handler
        defaultRouter.get(["/", "/health-check", "/status"], async (_req: Request, res: Response, _next: NextFunction) => {
            try {
                const healthStats = await this.healthCheck();
                if (healthStats.health) {
                    return res.json(healthStats);
                } else {
                    return res.status(503).send(healthStats);
                }
            } catch (err) {
                return res.status(503);
            }
        });

        this.express.use("/docs/asyncapi", express.static(path.join(__dirname, "../docs/asyncapi")));
        this.express.use("/", defaultRouter);
    };

    /**
     * Define error handling middleware
     */
    private errorHandlers = (): void => {
        this.express.use(sentry.Handlers.errorHandler({ shouldHandleError: () => true }) as express.ErrorRequestHandler);

        // Not found error - no route was matched
        this.express.use((_req: Request, _res: Response, next: NextFunction) => {
            next(new GeneralError("Not found", "App", undefined, 404));
        });

        // Error handler to catch all errors sent by routers (propagated through next(err))
        this.express.use((err: any, _req: Request, res: Response, _next: NextFunction) => {
            const error: IGolemioError = HTTPErrorHandler.handle(err, this.log);
            if (error) {
                this.log.silly("Error caught by the router error handler.");
                res.setHeader("Content-Type", "application/json; charset=utf-8");
                res.status(error.error_status || 500).send(error);
            }
        });
    };
}
