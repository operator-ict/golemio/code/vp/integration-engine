import { FatalError } from "@golemio/core/dist/shared/golemio-errors";
import { IQueueDefinition, AbstractWorker } from "@golemio/core/dist/integration-engine";

export type LoadModulesOutput = {
    [module: string]: {
        queueDefinitions: IQueueDefinition[];
        workers: Array<new () => AbstractWorker>;
    };
};

export class ModuleLoader {
    // See package.json for installed modules (@golemio/* packages)
    // and https://gitlab.com/operator-ict/golemio/code/modules for available modules
    private static modules = ["pid"];

    public static async loadModules(): Promise<LoadModulesOutput> {
        let output: LoadModulesOutput = {};

        for (const module of ModuleLoader.modules) {
            const pkg = `@golemio/${module}/dist/integration-engine`;

            output[module] = {
                queueDefinitions: [],
                workers: [],
            };

            try {
                const { queueDefinitions, workers } = await import(pkg);
                if (queueDefinitions) {
                    output[module].queueDefinitions = queueDefinitions;
                }

                if (workers) {
                    output[module].workers = workers;
                }
            } catch (err) {
                throw new FatalError(`Cannot import module ${pkg}.`, "ModuleLoader", err);
            }
        }

        return output;
    }
}
