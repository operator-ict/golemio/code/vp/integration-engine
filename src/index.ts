// load reflection
import "@golemio/core/dist/shared/_global";

// load telemetry before all deps
import { initTraceProvider } from "@golemio/core/dist/monitoring";
initTraceProvider();

// start app
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc";
import App from "./App";

IntegrationEngineContainer.registerSingleton(App);
IntegrationEngineContainer.resolve(App).start();
